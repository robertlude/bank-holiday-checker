// Note: if pasting this into Mimic, exclude the `module.exports = ` bit -- all
// that does is create a proper node module containing the function to run
// tests against

module.exports = function isBankHoliday(dateToEvaluate, dateFormat) {
  const moment = require("moment");

  const loggingEnabled = false

  let holidayProps = Object.create(null);
  holidayProps = {
    "New Year's Day": {
      dates: ["12/31", "01/01", "01/02"],
      staticDate: "01/01",
      dayOfTheWeek: null
    },
    "Martin Luther King Day": {
      dates: ["01/15", "01/16", "01/17", "01/18", "01/19", "01/20", "01/21"],
      dayOfTheWeek: "Monday"
    },
    "President's Day": {
      dates: ["02/15", "02/16", "02/17", "02/18", "02/19", "02/20", "02/21"],
      dayOfTheWeek: "Monday"
    },
    "Memorial Day": {
      dates: ["05/25", "05/26", "05/27", "05/28", "05/29", "05/30", "05/31"],
      dayOfTheWeek: "Monday"
    },
    "Independence Day": {
      dates: ["07/03", "07/04", "07/05"],
      staticDate: "07/04",
      dayOfTheWeek: null
    },
    "Labor Day": {
      dates: ["09/01", "09/02", "09/03", "09/04", "09/05", "09/06", "09/07"],
      dayOfTheWeek: "Monday"
    },
    "Columbus Day": {
      dates: ["10/08", "10/09", "10/10", "10/11", "10/12", "10/13", "10/14"],
      dayOfTheWeek: "Monday"
    },
    "Veterans Day": {
      dates: ["11/10", "11/11", "11/12"],
      staticDate: "11/11",
      dayOfTheWeek: null
    },
    "Thanksgiving Day": {
      dates: ["11/22", "11/23", "11/24", "11/25", "11/26", "11/27", "11/28"],
      dayOfTheWeek: "Thursday"
    },
    "Christmas Day": {
      dates: ["12/24", "12/25", "12/26"],
      staticDate: "12/25",
      dayOfTheWeek: null
    }
  };

  if (loggingEnabled)
    console.log(dateToEvaluate, dateFormat)

  const isValidDate = moment(dateToEvaluate, dateFormat, true).isValid();

  try {
    if (isValidDate) {

      let result = {
        isBankHoliday: false,
        holidayName: null,
        holidayObj: null
      };

      const theDateInQuestion = moment(dateToEvaluate, dateFormat).format("MM/DD");
      const theDateInQuestionDayOfWeek = moment(dateToEvaluate, dateFormat).format("dddd");
      const theDateInQuestionYear = moment(dateToEvaluate, dateFormat).format("YYYY");
      const holidays = Object.keys(holidayProps);
      let holidayInQuestion = null;
      let holidayName = null;

      if (loggingEnabled)
        console.log(theDateInQuestion, theDateInQuestionDayOfWeek, theDateInQuestionYear);

      holidays.forEach(holiday => {
        if (holidayProps[holiday].dates.includes(theDateInQuestion)) {
          holidayInQuestion = holidayProps[holiday];
          holidayName = holiday;

          if (loggingEnabled)
            console.log(`Holiday in Question: ${holiday}`, holidayInQuestion);
        }
      });

      const staticHoliday = holidayName && holidayInQuestion.staticDate;
      const floatingHoliday =
        holidayName &&
        !holidayInQuestion.staticDate &&
        holidayInQuestion.dayOfTheWeek === theDateInQuestionDayOfWeek;

      if (floatingHoliday) {
        result.isBankHoliday = true;
        result.holidayName = holidayName;
      }

      if (
        staticHoliday &&
        theDateInQuestionDayOfWeek !== "Saturday" &&
        theDateInQuestionDayOfWeek !== "Sunday"
      ) {
        //scenario one
        const exactMatchAndNotWeekend =
          theDateInQuestion === holidayInQuestion.staticDate;

        //helper variables for the other two scenarios
        let holidayFallsOnSaturday;
        if (theDateInQuestion === "12/31") {
          holidayFallsOnSaturday =
            moment(
              `${holidayInQuestion.staticDate}/${(
                parseFloat(theDateInQuestionYear) + 1
              ).toString()}`,
              "MM/DD/YYYY"
            ).format("dddd") === "Saturday";
        } else {
          holidayFallsOnSaturday =
            moment(
              `${holidayInQuestion.staticDate}/${theDateInQuestionYear}`,
              "MM/DD/YYYY"
            ).format("dddd") === "Saturday";
        }

        const holidayFallsOnSunday =
          moment(
            `${holidayInQuestion.staticDate}/${theDateInQuestionYear}`,
            "MM/DD/YYYY"
          ).format("dddd") === "Sunday";

        //scenario two
        const saturdayHolidayObservedFriday =
          holidayFallsOnSaturday &&
          theDateInQuestion === holidayInQuestion.dates[0];
        //scenario three
        const sundayHolidayObservedMonday =
          holidayFallsOnSunday &&
          theDateInQuestion === holidayInQuestion.dates[2];
        if (
          exactMatchAndNotWeekend ||
          saturdayHolidayObservedFriday ||
          sundayHolidayObservedMonday
        ) {
          result.isBankHoliday = true;
          result.holidayName = holidayName;
          result.holidayObj = holidayInQuestion;
        }
      }
      return result;
    } else {
      throw `Invalid Date: ${dateToEvaluate}`;
    }
  } catch (e) {
    // Always log exception

    console.log(e);
  }
}
