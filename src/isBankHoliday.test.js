/// External Dependencies

const moment = require('moment')

/// Internal Dependencies

const isBankHoliday                       = require('./isBankHoliday.js')
    , randomDateWithWeekday               = require('./testSupport/randomDateWithWeekday.js')
    , randomDateWithWeekdayInWeek         = require('./testSupport/randomDateWithWeekdayInWeek.js')
    , randomNonBankHolidayDateWithWeekday = require('./testSupport/randomNonBankHolidayDateWithWeekday.js')

    , {
        DATE_FORMAT,

        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,

        DATE_BANK_HOLIDAYS,
        DAY_OF_WEEK_BANK_HOLIDAYS,
      } = require('./testSupport/constants.js')

/// Tests

const MIDWEEK = [
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
      ]

    , weekdayPositiveTests = (holidayName, holidayDate, weekday, additionalTests) => {
        describe(`when the given date is ${holidayName}`, () => {
          test(`returns an object with \`isBankHoliday\` set to \`true\``, () => {
            const dateString = randomDateWithWeekday(holidayDate, weekday)

                , result = isBankHoliday(dateString, DATE_FORMAT)

            expect(result.isBankHoliday).toBe(true)
          })

          test(`returns an object with \`holidayName\` set to \`"${holidayName}"\``, () => {
            const dateString = randomDateWithWeekday(holidayDate, weekday)

                , result = isBankHoliday(dateString, DATE_FORMAT)

            expect(result.holidayName).toBe(holidayName)
          })
        })

        if (additionalTests)
          additionalTests(holidayName, holidayDate)
      }

    , weekdayNegativeTests = weekday => {
        describe(`when the given date is not a holiday`, () => {
          test(`returns an object with \`isBankHoliday\` set to \`false\``, () => {
            const dateString = randomNonBankHolidayDateWithWeekday(weekday)

                , result = isBankHoliday(dateString, DATE_FORMAT)

            expect(result.isBankHoliday).toBe(false)
          })
        })
      }

    , checkAdjacent = (holidayName, holidayDate, adjacentDayOfWeek, label, dayDelta) => {
        describe(`when the ${label} date is ${holidayName}`, () => {
          test(`returns an object with \`isBankHoliday\` set to \`true\``, () => {
            const previousDateString = randomDateWithWeekday(
                                         holidayDate,
                                         adjacentDayOfWeek,
                                       )

                , dateString = moment(previousDateString, DATE_FORMAT)
                                 .add(-dayDelta, 'days')
                                 .format(DATE_FORMAT)

                , result = isBankHoliday(dateString, DATE_FORMAT)

            expect(result.isBankHoliday).toBe(true)
          })

          test(`returns an object with \`holidayName\` set to \`"${holidayName}"\``, () => {
            const previousDateString = randomDateWithWeekday(
                                         holidayDate,
                                         adjacentDayOfWeek,
                                       )

                , dateString = moment(previousDateString, DATE_FORMAT)
                                 .add(-dayDelta, 'days')
                                 .format(DATE_FORMAT)

                , result = isBankHoliday(dateString, DATE_FORMAT)

            expect(result.holidayName).toBe(holidayName)
          })
        })
      }

    , checkPreviousSunday = (holidayName, holidayDate) =>
        checkAdjacent(holidayName, holidayDate, SUNDAY, 'previous', -1)

    , checkNextSaturday = (holidayName, holidayDate) =>
        checkAdjacent(holidayName, holidayDate, SATURDAY, 'next', 1)

describe(`when the given date is a ${MONDAY}`, () => {
  weekdayNegativeTests(MONDAY)

  Object
    .entries(DATE_BANK_HOLIDAYS)
    .forEach(([holidayName, holidayDate]) =>
      weekdayPositiveTests(
        holidayName,
        holidayDate,
        MONDAY,
        checkPreviousSunday,
      )
    )
})

MIDWEEK.forEach(weekday => {
  describe(`when the given date is a ${weekday}`, () => {
    weekdayNegativeTests(weekday)

    Object
      .entries(DATE_BANK_HOLIDAYS)
      .forEach(([holidayName, holidayDate]) => {
        weekdayPositiveTests(holidayName, holidayDate, weekday)
      })
  })
})

describe(`when the given date is a ${FRIDAY}`, () => {
  weekdayNegativeTests(MONDAY)

  Object
    .entries(DATE_BANK_HOLIDAYS)
    .forEach(([holidayName, holidayDate]) =>
      weekdayPositiveTests(
        holidayName,
        holidayDate,
        MONDAY,
        checkNextSaturday,
      )
    )
})

Object
  .entries(DAY_OF_WEEK_BANK_HOLIDAYS)
  .forEach(([holidayName, {dayOfWeek, firstPossibleDate}]) => {
    describe(`when the given date is ${holidayName}`, () => {
      test(`returns an object with \`isBankHoliday\` set to \`true\``, () => {
        const dateString = randomDateWithWeekdayInWeek(firstPossibleDate, dayOfWeek)

            , result = isBankHoliday(dateString, DATE_FORMAT)

        expect(result.isBankHoliday).toBe(true)
      })

      test(`returns an object with \`holidayName\` set to \`'${holidayName}'\``, () => {
        const dateString = randomDateWithWeekdayInWeek(firstPossibleDate, dayOfWeek)

            , result = isBankHoliday(dateString, DATE_FORMAT)

        expect(result.holidayName).toBe(holidayName)
      })
    })
  })
