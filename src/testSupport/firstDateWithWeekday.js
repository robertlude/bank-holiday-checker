/// External Dependencies

const moment = require('moment')

/// Internal Dependencies

const randomYear      = require('./randomYear.js')
    , { DATE_FORMAT } = require('./constants.js')

/// Module

module.exports = (dayOfWeek, firstPossibleDate) => {
  let result = moment(firstPossibleDate, DATE_FORMAT)

  while (result.format('dddd') != dayOfWeek)
    result = result.add(1, 'days')

  return result.format(DATE_FORMAT)
}
