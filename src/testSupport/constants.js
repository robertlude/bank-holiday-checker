/// Module

module.exports = {
  DATE_FORMAT: 'YYYY-M-D',

  SUNDAY:    'Sunday',
  MONDAY:    'Monday',
  TUESDAY:   'Tuesday',
  WEDNESDAY: 'Wednesday',
  THURSDAY:  'Thursday',
  FRIDAY:    'Friday',
  SATURDAY:  'Saturday',

  DATE_BANK_HOLIDAYS: {
    'Christmas Day':    '12-25',
    'Independence Day': '7-4',
    'New Year\'s Day':  '1-1',
    'Veterans Day':     '11-11',
  },

  DAY_OF_WEEK_BANK_HOLIDAYS: {
    'Martin Luther King Day': {
      dayOfWeek:         'Monday',
      firstPossibleDate: '1-15',
    },
    'President\'s Day': {
      dayOfWeek:         'Monday',
      firstPossibleDate: '2-15',
    },
    'Memorial Day': {
      dayOfWeek:         'Monday',
      firstPossibleDate: '5-25',
    },
    'Labor Day': {
      dayOfWeek:         'Monday',
      firstPossibleDate: '9-1',
    },
    'Columbus Day': {
      dayOfWeek:         'Monday',
      firstPossibleDate: '10-8',
    },
    'Thanksgiving Day': {
      dayOfWeek:         'Thursday',
      firstPossibleDate: '11-22',
    },
  },
}
