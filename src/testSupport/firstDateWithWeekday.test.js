/// External Dependencies

const moment = require('moment')

/// Internal Dependencies

const firstDateWithWeekday  = require('./firstDateWithWeekday.js')
    , randomDateWithWeekday = require('./randomDateWithWeekday.js')
    , randomMonthDay        = require('./randomMonthDay.js')
    , {
        DATE_FORMAT,

        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
      } = require('./constants.js')

/// Module

describe(`when given a date that is the given weekday`, () => {
  test(`returns the same date`, () => {
    const targetDate = randomDateWithWeekday(randomMonthDay(), SUNDAY)

        , result = firstDateWithWeekday(SUNDAY, targetDate)

    expect(result).toBe(targetDate)
  })
})

describe(`when given a date that is the day before the given weekday`, () => {
  test(`returns the next day's date `, () => {
    const givenDate = randomDateWithWeekday(randomMonthDay(), SUNDAY)

        , targetDate = moment(givenDate, DATE_FORMAT)
                         .add(1, 'days')
                         .format(DATE_FORMAT)

        , result = firstDateWithWeekday(MONDAY, givenDate)

    expect(result).toBe(targetDate)
  })
})

describe(`when given a date that is two days before the given weekday`, () => {
  test(`returns the date two days from the given date`, () => {
    const givenDate = randomDateWithWeekday(randomMonthDay(), SUNDAY)

        , targetDate = moment(givenDate, DATE_FORMAT)
                         .add(2, 'days')
                         .format(DATE_FORMAT)

        , result = firstDateWithWeekday(TUESDAY, givenDate)

    expect(result).toBe(targetDate)
  })
})

describe(`when given a date that is three days before the given weekday`, () => {
  test(`returns the date three days from the given date`, () => {
    const givenDate = randomDateWithWeekday(randomMonthDay(), SUNDAY)

        , targetDate = moment(givenDate, DATE_FORMAT)
                         .add(3, 'days')
                         .format(DATE_FORMAT)

        , result = firstDateWithWeekday(WEDNESDAY, givenDate)

    expect(result).toBe(targetDate)
  })
})

describe(`when given a date that is four days before the given weekday`, () => {
  test(`returns the date four days from the given date`, () => {
    const givenDate = randomDateWithWeekday(randomMonthDay(), SUNDAY)

        , targetDate = moment(givenDate, DATE_FORMAT)
                         .add(4, 'days')
                         .format(DATE_FORMAT)

        , result = firstDateWithWeekday(THURSDAY, givenDate)

    expect(result).toBe(targetDate)
  })
})

describe(`when given a date that is five days before the given weekday`, () => {
  test(`returns the date five days from the given date`, () => {
    const givenDate = randomDateWithWeekday(randomMonthDay(), SUNDAY)

        , targetDate = moment(givenDate, DATE_FORMAT)
                         .add(5, 'days')
                         .format(DATE_FORMAT)

        , result = firstDateWithWeekday(FRIDAY, givenDate)

    expect(result).toBe(targetDate)
  })
})

describe(`when given a date that is six days before the given weekday`, () => {
  test(`returns the date six days from the given date`, () => {
    const givenDate = randomDateWithWeekday(randomMonthDay(), SUNDAY)

        , targetDate = moment(givenDate, DATE_FORMAT)
                         .add(6, 'days')
                         .format(DATE_FORMAT)

        , result = firstDateWithWeekday(SATURDAY, givenDate)

    expect(result).toBe(targetDate)
  })
})
