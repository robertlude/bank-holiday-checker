/// External Dependencies

const moment = require('moment')

/// Internal Dependencies

const randomYear      = require('./randomYear.js')
    , { DATE_FORMAT } = require('./constants.js')

/// Module

module.exports = (firstPossibleDate, weekday) => {
  const year = randomYear()

  let date = moment(`${year}-${firstPossibleDate}`)

  while (true) {
    if (date.format('dddd') == weekday)
      return date.format(DATE_FORMAT)

    date = date.add(1, 'days')
  }
}
