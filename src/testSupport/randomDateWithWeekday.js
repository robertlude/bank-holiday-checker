/// External Dependencies

const moment = require('moment')

/// Internal Dependencies

const randomYear      = require('./randomYear.js')
    , { DATE_FORMAT } = require('./constants.js')

/// Module

module.exports = (monthDay, weekday) => {
  while (true) {
    const date = `${randomYear()}-${monthDay}`

        , result = moment(date, DATE_FORMAT)

    if (result.format('dddd') == weekday)
      return date
  }
}
