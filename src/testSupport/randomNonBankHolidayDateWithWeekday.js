/// External Dependencies

const moment = require('moment')

/// Internal Dependencies

const firstDateWithWeekday = require('./firstDateWithWeekday.js')
    , randomMonthDay       = require('./randomMonthDay.js')
    , randomYear           = require('./randomYear.js')

const {
  DATE_FORMAT,

  DATE_BANK_HOLIDAYS,
  DAY_OF_WEEK_BANK_HOLIDAYS,
} = require('./constants.js')

/// Module

const DATE_BANK_HOLIDAY_DATES = Object.values(DATE_BANK_HOLIDAYS)

module.exports = weekday => {
  const year = randomYear()

  while (true) {
    const monthDay = randomMonthDay()

        , dateString = `${year}-${monthDay}`

    if (DATE_BANK_HOLIDAY_DATES.includes(monthDay))
      continue

    const dayOfWeekHolidays = Object
                                .entries(DAY_OF_WEEK_BANK_HOLIDAYS)
                                .map(([holiday, {dayOfWeek, firstPossibleDate}]) => {
                                  if (dayOfWeek != weekday)
                                    return false

                                  const dayOfWeekHoliday = firstDateWithWeekday(dayOfWeek, `${year}-${firstPossibleDate}`)

                                  return dayOfWeekHoliday == dateString
                                })
                                .filter(value => value)

    const momentWeekday = moment(`${year}-${monthDay}`, DATE_FORMAT).format('dddd')

    if (dayOfWeekHolidays.length)
      continue

    const date = `${year}-${monthDay}`

    if (moment(date, DATE_FORMAT).format('dddd') != weekday)
      continue

    return date
  }
}
