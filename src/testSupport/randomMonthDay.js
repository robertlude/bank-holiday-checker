/// External Dependencies

const DAY_COUNT = [
  31, 28, 31, 30, 31, 30,
  31, 31, 30, 31, 30, 31,
]

module.exports = () => {
  const month = Math.floor(Math.random() * 12)

      , day = Math.floor(Math.random() * DAY_COUNT[month])

  return `${1 + month}-${1 + day}`
}
